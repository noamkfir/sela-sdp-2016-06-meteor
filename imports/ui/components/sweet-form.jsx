import React, { Component } from 'react';

export default class SweetForm extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            message: ''
        };
    }

    handleSubmit(e) {
        e.preventDefault();

        const created = new Date();
        const creator = this.state.username;
        const text = this.state.message;

        const sweet = {created, creator, text};
        this.props.addSweet(sweet);

        this.setState({text: ''});
    }

    handleUsernameChange(e) {
        const username = e.target.value;
        this.setState({username});
    }

    handleMessageChange(e) {
        const message = e.target.value;
        this.setState({message});
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div>
                    <label htmlFor="username">Who are you? </label>
                    <input
                        id="username"
                        value={this.state.username}
                        onChange={this.handleUsernameChange.bind(this)}
                    />
                </div>
                <div>
                    <label htmlFor="message">Say something sweet: </label>
                    <input
                        id="message"
                        value={this.state.message}
                        onChange={this.handleMessageChange.bind(this)}
                    />
                </div>
                <div>
                    <button type="submit">Go!</button>
                </div>
            </form>
        );
    }
}
