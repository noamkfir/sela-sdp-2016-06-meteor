import React from 'react';

export default ({sweets}) => (
    <ul>
        {
            sweets.map((sweet, index) => (
                <li key={index}>
                    <div>{sweet.text}</div>
                    <div>{sweet.creator} - {sweet.created.toDateString()}</div>
                </li>
            ))
        }
    </ul>
);
