import React from 'react';
import SweetContainer from '/imports/ui/containers/sweet-container';

export default () => (
    <SweetContainer />
);
