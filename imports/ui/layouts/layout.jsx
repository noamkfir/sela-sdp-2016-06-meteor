import React from 'react';
import Header from '../components/header';
import Footer from '../components/footer';

export default ({content}) => (
    <div>
        <header>
            <Header title="switter" />
        </header>
        <main>
            {content}
        </main>
        <footer>
            <Footer />
        </footer>
    </div>
);
