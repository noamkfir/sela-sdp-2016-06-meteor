import React, { Component } from 'react';
import ReactMixin from 'react-mixin';
import { ReactMeteorData } from 'meteor/react-meteor-data';
import Sweets from '/imports/api/sweets';
import SweetList from '/imports/ui/components/sweet-list.jsx';
import SweetForm from '/imports/ui/components/sweet-form';

export default class SweetContainer extends Component {
    getMeteorData() {
        const sweets = Sweets.find().fetch();
        return {sweets};
    }

    addSweet(sweet) {
        Sweets.insert(sweet);
    }

    render() {
        return (
            <div>
                <SweetForm addSweet={this.addSweet.bind(this)} />
                <SweetList sweets={this.data.sweets} />
            </div>
        );
    }
}
ReactMixin(SweetContainer.prototype, ReactMeteorData);
