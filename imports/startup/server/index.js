import Sweets from '/imports/api/sweets';
import { createSweets } from './initialData';

if (Sweets.find().count() === 0) {
    createSweets().forEach(sweet => Sweets.insert(sweet));
}
