import React from 'react';
import { mount } from 'react-mounter';
import Layout from '/imports/ui/layouts/layout';
import Content from '/imports/ui/components/content';

mount(Layout, {
    content: <Content />
});
